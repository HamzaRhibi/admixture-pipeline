#!/usr/bin/env python

"""
Purpose: Reads in fam file and 1000genomes ped file to create a
    .pop file as input for ADMIXTURE
Author: Ryan Dhindsa
"""
# Imports --------------------------------------------------------------------
import argparse


# Classes --------------------------------------------------------------------


# Functions ------------------------------------------------------------------
def read_1000g_ped(ped):
    """
    Reads in 1000g ped and returns a dict with IDs and ancestries
    :param ped: path to ped file
    :return:
    """
    ethnicity_dict = {}

    for line in ped:
        if line.startswith("Family ID"):
            continue
        spl = line.strip().split("\t")

        ind_id, ethnicity = spl[1], spl[6]

        ethnicity_dict[ind_id] = ethnicity

    return ethnicity_dict


def read_fam(fam, ped_dict, outfile):
    """
    Reads in fam file and writes ethnicities to .pop file
    :param fam: path to fam file
    :param ped_dict: dict containing individual IDs and their ethnicities
    :param outfile: path to output file
    :return: None
    """
    for line in fam:
        spl = line.strip().split()
        ind = spl[0]

        if ind in ped_dict.keys():
            ethnicity = ped_dict[ind]

        else:
            ethnicity = "-"

        outfile.write("{}\n".format(ethnicity))


# Main -----------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("-p", "--ped",
                        help="Input 1000 genomes .ped file",
                        required=True,
                        type=argparse.FileType("r"))

    parser.add_argument("-f", "--fam",
                        help="Input fam file (plink format)",
                        required=True,
                        type=argparse.FileType("r"))

    parser.add_argument("-o", "--out",
                        help="Output .pop file",
                        required=True,
                        type=argparse.FileType("w"))

    args = parser.parse_args()

    ped_dict = read_1000g_ped(args.ped)
    read_fam(args.fam, ped_dict, args.out)

