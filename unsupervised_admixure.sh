#!/bin/bash

##################################Unsupervised Admixure pipeline###########################
# Purpose : automating manual work and analysis
# requirements : admixture installed,plink,python3 installed,make_pop.py script,1000g_labels_flle
#Author : Hamza Rhibi 
#Notes: the 1000g labels files generation is done separately,starting from the bed pruned file
###########################################################################################
#running exemple : ./unsupervised_admixure.sh -b=GIG1944_maf001_pruned.bed -p=20130606_g1k.ped -t=8 -n=GIG1944 
if [[ $# -ne 4 ]] ; then
    echo 'missing argument'
    exit 2
fi
for i in "$@"
do
case $i in
    -t=*|--threads=*)
    nb_threads="${i#*=}"
    ;;
    -b=*|--bed*)
    bed="${i#*=}"
    ;;
   
    -p=*|--ped=*)
    ped="${i#*=}"
    ;;
     -n=*|--name=*)
    name="${i#*=}"
    ;;
    *)
            # unknown option
            echo 'Unknown options'
            exit 2
    ;;
esac
done

echo nbTreads=${nb_threads} 
echo bedFile=${bed}
echo PedFile=${ped}
echo indiv_name=${name}
#####################checking extentions########################
if [ ${bed: -4} != ".bed" ]; then
    echo 'bed file should have .bed extention'
    exit 2
fi
if [ ${ped: -4} != ".ped" ]; then
    echo 'ped file should have .ped extention'
    exit 2
fi
################################################################
python make_pop.py -p ${ped} -f ${bed%.*}.fam -o ${bed%.*}.pop;
admixture ${bed} 10 -j${nb_threads} | tee ${bed%.*}_unsup_K10.out #the choice of k=10 has been done using
                                                                                             #CrossValidation using this cmd 
                                                                                             #for K in {1..26}; do admixture --cv 1000g.bed $K -jN | tee log${K}.out; done
paste -d " " 1000g_labels ${bed%.*}.10.Q > ${bed%.*}_tmp;
python generate_plots.py ${bed%.*}_tmp ${name};
echo 'Done!';
