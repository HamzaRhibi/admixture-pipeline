#!/usr/bin/env python

"""
Purpose: Read unsupervised Admixture output and generate plots 
         based on that for the reference set and for the inidividual
Author: Rhibi Hamza
Estimated Execution time : 
real    1m0.685s
user    0m59.615s
sys     0m0.671s
"""
# Imports --------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys
import matplotlib

# Classes --------------------------------------------------------------------
# Functions ------------------------------------------------------------------
def main():
    admixure_df=pd.read_table(input_file,sep=' ',header=None)
    columns=['ID','population code','continent','k0','k1','k2','k3','k4','k5','k6','k7','k8','k9']
    admixure_df.columns=columns
    unknown=admixure_df[-1:].copy()
    admixure_df=admixure_df[:-1]
    admixure_df = admixure_df.sort_values(by=['continent', 'population code'])
    admixure_df.reset_index(inplace=True,drop=True)
    frequencies = [admixure_df[admixure_df['continent']=='AFR'].shape[0],admixure_df[admixure_df['continent']=='AMR'].shape[0],admixure_df[admixure_df['continent']=='EAS'].shape[0],admixure_df[admixure_df['continent']=='EUR'].shape[0],admixure_df[admixure_df['continent']=='SAS'].shape[0]]# calculate the frequency of each continent 
    best_predictions_df = unknown.drop(['ID','population code','continent'],axis=1).sort_values(unknown.last_valid_index(), axis=1)
    best_predictions_df=best_predictions_df[best_predictions_df.columns[-3:]]
    s=best_predictions_df.columns
    Title_prediction_plot=s.values[0]+' : '+str(round(unknown[s.values[0]][2504]*100,2))+'%, '+s.values[1]+' : '+str(round(unknown[s.values[1]][2504]*100,2))+'%, '+s.values[2]+' : '+str(round(unknown[s.values[2]][2504]*100,2))+'%'
    frequencies = [admixure_df[admixure_df['continent']=='AFR'].shape[0],admixure_df[admixure_df['continent']=='AMR'].shape[0],admixure_df[admixure_df['continent']=='EAS'].shape[0],admixure_df[admixure_df['continent']=='EUR'].shape[0],admixure_df[admixure_df['continent']=='SAS'].shape[0]]# calculate the frequency of each continent 
    ### bock used to fix  K's 
    '''print(admixure_df['k0'].idxmax())# 30
    print(admixure_df['k1'].idxmax())# 1612
    print(admixure_df['k2'].idxmax())# 176
    print(admixure_df['k3'].idxmax())# 159
    print(admixure_df['k4'].idxmax())# 33
    print(admixure_df['k5'].idxmax())# 369
    print(admixure_df['k6'].idxmax())# 1512
    print(admixure_df['k7'].idxmax())# 375
    print(admixure_df['k8'].idxmax())# 752
    print(admixure_df['k9'].idxmax())# 157'''  

    #based on the fixed K's reorder the columns
    supposed_k0=admixure_df.iloc[[30]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k1=admixure_df.iloc[[1612]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k2=admixure_df.iloc[[176]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k3=admixure_df.iloc[[159]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k4=admixure_df.iloc[[33]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k5=admixure_df.iloc[[369]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k6=admixure_df.iloc[[1512]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k7=admixure_df.iloc[[375]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k8=admixure_df.iloc[[752]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    supposed_k9=admixure_df.iloc[[157]].drop(['ID','population code','continent'],axis=1).idxmax(axis=1).all()
    columnsTitles=['ID','population code','continent',supposed_k0,supposed_k1,supposed_k2,supposed_k3,supposed_k4,supposed_k5,supposed_k6,supposed_k7,supposed_k8,supposed_k9]
    admixure_df=admixure_df.reindex(columns=columnsTitles)
    ##end reordering

    plt.rcParams["figure.figsize"] = [50,10]
    #Creates two subplots and unpacks the output array immediately
    N = admixure_df.shape[0]
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw = {'width_ratios':[3, 1]})
    ax1.set_ylabel('percentage',fontsize=20)
    ax1.set_title('Unsupervised Admixure',fontsize=30)
    labels_positions=[]
    acc_sum=0
    for f in frequencies:
        labels_positions.append(acc_sum+f/2)
        acc_sum+=f

    k0 = admixure_df['k0'].values*100 #red
    k1 = admixure_df['k1'].values*100 #blue
    k2 = admixure_df['k2'].values*100 #cian
    k3 = admixure_df['k3'].values*100 #green
    k4 = admixure_df['k4'].values*100 #cyan
    k5 = admixure_df['k5'].values*100 #darkorange
    k6 = admixure_df['k6'].values*100 #black
    k7 = admixure_df['k7'].values*100 #magenta
    k8 = admixure_df['k8'].values*100 #lime
    k9 = admixure_df['k9'].values*100 #silver
    ind = np.arange(N)    # the x locations for the groups

    p1 = ax1.bar(ind, tuple(k0) ,  color='r')
    p2 = ax1.bar(ind, tuple(k1) ,
                bottom=k0,  color='b')
    p3 = ax1.bar(ind, tuple(k2) ,
                bottom=k1+k0,  color='c')
    p4 = ax1.bar(ind, tuple(k3) ,
                bottom=k1+k2+k0,  color='g')
    p5 = ax1.bar(ind, tuple(k4) ,
                bottom=k1+k2+k0+k3,  color='darkorange')
    p6 = ax1.bar(ind, tuple(k5) ,
                bottom=k1+k2+k0+k3+k4,  color='k')
    p7 = ax1.bar(ind, tuple(k6) ,
                bottom=k1+k2+k0+k3+k4+k5,  color='m')
    p8 = ax1.bar(ind, tuple(k7) ,
                bottom=k1+k2+k0+k3+k4+k5+k6,  color='lime')
    p9 = ax1.bar(ind, tuple(k8) ,
                bottom=k1+k2+k0+k3+k4+k5+k6+k7,  color='silver')
    p10 = ax1.bar(ind, tuple(k9) ,
                bottom=k1+k2+k0+k3+k4+k5+k6+k7+k8,  color='sienna')
    ax1.set_xticks(labels_positions)
    ax1.set_xticklabels(['AFR','AMR','EAS','EUR','SAS'])
    ax1.set_yticks(np.arange(0, 110, 10))
    ax1.legend((p1[0], p2[0],p3[0],p4[0], p5[0],p6[0],p7[0], p8[0],p9[0],p10[0]), ('k0', 'k1','k3','k4', 'k5','k6','k7', 'k8','k9'))

    N = unknown.shape[0]
    k0 = unknown['k0'].values*100 #red
    k1 = unknown['k1'].values*100 #blue
    k2 = unknown['k2'].values*100 #cian
    k3 = unknown['k3'].values*100 #green
    k4 = unknown['k4'].values*100 #cyan
    k5 = unknown['k5'].values*100 #darkorange
    k6 = unknown['k6'].values*100 #black
    k7 = unknown['k7'].values*100 #magenta
    k8 = unknown['k8'].values*100 #lime
    k9 = unknown['k9'].values*100 #silver
    ind = np.arange(N)    # the x locations for the groups

    p1 = ax2.bar(ind, tuple(k0) ,  color='r')
    p2 = ax2.bar(ind, tuple(k1) ,
                bottom=k0,  color='b')
    p3 = ax2.bar(ind, tuple(k2) ,
                bottom=k1+k0,  color='c')
    p4 = ax2.bar(ind, tuple(k3) ,
                bottom=k1+k2+k0,  color='g')
    p5 = ax2.bar(ind, tuple(k4) ,
                bottom=k1+k2+k0+k3,  color='darkorange')
    p6 = ax2.bar(ind, tuple(k5) ,
                bottom=k1+k2+k0+k3+k4,  color='k')
    p7 = ax2.bar(ind, tuple(k6) ,
                bottom=k1+k2+k0+k3+k4+k5,  color='m')
    p8 = ax2.bar(ind, tuple(k7) ,
                bottom=k1+k2+k0+k3+k4+k5+k6,  color='lime')
    p9 = ax2.bar(ind, tuple(k8) ,
                bottom=k1+k2+k0+k3+k4+k5+k6+k7,  color='silver')
    p10 = ax2.bar(ind, tuple(k9) ,
                bottom=k1+k2+k0+k3+k4+k5+k6+k7+k8,  color='sienna')
    ax2.set_title('Top 3 Prediction : '+ Title_prediction_plot,fontsize=30)
    ax2.set_xticklabels([indiv_name])
    ax2.set_xticks(np.arange(1))

    ax2.set_yticks(np.arange(0, 110, 10))
    ax2.legend((p1[0], p2[0],p3[0],p4[0], p5[0],p6[0],p7[0], p8[0],p9[0],p10[0]), ('k0', 'k1','k3','k4', 'k5','k6','k7', 'k8','k9'))

    plt.savefig(str(indiv_name)+'.png')




# Main -----------------------------------------------------------------------

if __name__ == "__main__":
    indiv_name= sys.argv[2]
    input_file= sys.argv[1]
    main()


