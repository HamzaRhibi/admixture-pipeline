# Admixture pipeline
A full pipeline for admixture software

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Admixture software
Python 3
```

### Installing

A step by step series of examples that tell you how to get a development env running

Download and install admixture 

follow this [link](http://software.genetics.ucla.edu/admixture/)


Add Admixture pipeline folder to the path variable 

```
export PATH=/path/to/admixture:$PATH

```
Make unsupervised_admixure.sh script executable

```
chmod +x /path/to/admixture/unsupervised_admixure.sh

```
## Running 

An exeuction sample :

```
unsupervised_admixure.sh -t=[number of threads default:4] -b=[bed file] -p=20130606_g1k.ped -n=[Name of the indivudal ex:GIG1916]
```
### Exemple : 
```
unsupervised_admixure.sh -b=GIG1944_maf001_pruned.bed -p=20130606_g1k.ped -t=8 -n=GIG1944 
```

## Built With

* [Visual code](https://code.visualstudio.com/) 

## Contributing

## Authors

* **Rhibi Hamza** - *Initial work* - [Github account](https://github.com/Sigm0oid)



